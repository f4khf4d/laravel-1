<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.form');
    }

    public function welcome(Request $request){
        //dd($request->all());
        $fnama = $request->fn;
        $lnama = $request->ln;

        return view('halaman.home', compact('fnama', 'lnama'));
    }
}
