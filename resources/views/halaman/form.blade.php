@extends('layout.master')

@section('judul')
Register
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf

        <label for="">First Name</label> <br><br>
        <input type="text" name="fn"> <br><br>

        <label for="">Last Name</label> <br><br>
        <input type="text" name="ln"> <br><br>

        <label>Gender:</label> <br><br>
        <input type="radio" name="g" value="M"> Male <br>
        <input type="radio" name="g" value="F"> Female <br>
        <input type="radio" name="g" value="O"> Other <br><br>

        <label>Nationality:</label> <br><br>
        <select name="n">
            <option value="1">Indonesia</option>
            <option value="1">Amerika</option>
            <option value="1">Inggris</option>
        </select> <br><br>

        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="ls"> Bahasa Indonesia <br>
        <input type="checkbox" name="ls"> English <br>
        <input type="checkbox" name="ls"> Other <br><br>

        <label>Bio:</label> <br><br>
        <textarea name="b" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">
    </form>
@endsection