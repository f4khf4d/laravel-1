@extends('layout.master')

@section('judul')
Home
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$fnama}} {{$lnama}}</h1>
    <p>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</p>
@endsection